<?php

if (isset($_GET['key'])) {
	$f = [
		'config'         => '/home/masterpoll-documents/bot-config.php',
		'bot.dir'        => '/var/www/bot-clones/',
		'jpgraph.dir'    => '/home/masterpoll-documents/lib/chart/',
		'functions'      => '/var/www/bot/hwgxh7pollbotwg47ej2/functions.php',
		'anti-flood'     => '/var/www/bot/hwgxh7pollbotwg47ej2/antiflood.php',
		'database'       => '/var/www/bot/hwgxh7pollbotwg47ej2/database.php',
		'plugin_manager' => '/var/www/bot/hwgxh7pollbotwg47ej2/plugin_manager.php',
		'plugins.dir'    => '/var/www/bot/hwgxh7pollbotwg47ej2/plugins',
		'plugins'        => '/var/www/bot/hwgxh7pollbotwg47ej2/plugins.json',
		'thumb_logo'     => '/home/masterpoll-documents/thumb_logo.jpeg',
		'affiliate'      => '/home/masterpoll-documents/affiliates.json',
		'languages'      => 'languages.json',
		'class'          => '/home/masterpoll-documents/bots/PollCreator-Class.php'
	];
	$api = urldecode($_GET['key']);
	$botID = str_replace('bot', '', explode(":", $api)[0]);
	require $f['config'];
	require("/home/masterpoll-documents/bots/RedisConnection.php");
} else {
	http_response_code(400);
	die;
}
ignore_user_abort(true);
$ob = ob_start();
$times['start'] = microtime(true);
$puginp = "index.php/start";
$admins = $config['admins'];
$password = urldecode($_GET['password']);
$time_run = date('d-m-y-h-i-s');
if (!file_exists("/var/log/masterpoll/bot$botID.log")) {
	file_put_contents("/var/log/masterpoll/bot$botID.log", '');
}
ini_set('error_log', "/var/log/masterpoll/bot$botID.log");
date_default_timezone_set("UTC");
$times['telegram_update'] = microtime(true);
$content = file_get_contents("php://input");
if (!$content) {
	$update = false;
	die("Empty update ._.");
} else {
	$update = json_decode($content, true);
	$rupdate = $update;
}
$times['functions'] = microtime(true);
$ch_api = curl_init();
require($f['functions']);
if (in_array($userID, $admins)) {
	$isadmin = true;
	/*if ($cmd == "test") {
		sm($chatID, "✅ OK");
		die;
	}*/
} else {
	$isadmin = false;
}
if ($config['usa_il_db'] and $config['usa_redis']) {
	require($f['database']);
} else {
	$config['json_payload'] = true;
	$config['response'] = false;
	if ($cbdata) {
		cb_reply($cbid, "⚠️ Bot offline", false);
	} elseif ($typechat == "private") {
		sm($chatID, "⚠️ Bot offline");
	}
	die;
}
$times['plugins'] = microtime(true);
$plugins = json_decode(file_get_contents($f['plugins']), true);
if (isset($plugins['gestione.php'])) unset($plugins['gestione.php']);
foreach ($plugins as $pl => $on) {
	$pluginp = $pl;
	if ($on) {
		if (file_exists($f['plugins.dir'] . '/' . $pl)) {
			require($f['plugins.dir'] . '/' . $pl);
		} else {
			call_error("Il plugin '" . code($pl) . "' non è stato trovato in " . code($f['plugins.dir']));
		}
	}
}

?>